/*
 * daemon.cpp
 *
 *  Created on: 31.03.2016
 *      Author: Pavel Tsytovich
 *
 */
#include <unistd.h>
#include <iostream>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "../contrib/gpio.h"

using namespace ::std;

volatile bool isExit = false;

/**
 * signal`s handler
 */
void signal_handler(int signo) {
	isExit = true;
}

void printUsage(const char *msg = "usage: gpiodaemon [-d |-s] | -p pin") {
	cout << msg << endl;
}

void setSignals() {
	struct sigaction act = { 0 };
	act.sa_handler = signal_handler;
	sigemptyset(&act.sa_mask);
	sigaddset(&act.sa_mask, SIGINT);
	sigaddset(&act.sa_mask, SIGTERM);
	sigaction(SIGINT, &act, 0);
}

bool getCommandLineParameter(int argc, bool isDaemon, char** argv,
		bool& isStandalone,int& port) {
	port = 0;
	if (argc < 2) {
		printUsage();
		exit(1);
	}
	for (int i = 1; i < argc; i++) {
		if (argv[i][0] != '-')
			continue;

		if (argv[i][1] == 'd') {
			isDaemon = true;
		} else if (argv[i][1] == 's') {
			isStandalone = true;
		}
		else if(argv[i][1] == 'p') {
			port = atoi(argv[i]+3);
		}
	}
	return isDaemon;
}

int main(int argc,char **argv) {
	bool isDaemon = false;
	bool isStandalone = false;
	int port = 0;
    char portNumber[3];

	setSignals();

	isDaemon = getCommandLineParameter(argc, isDaemon, argv, isStandalone,port);
	sprintf(portNumber,"%d",port);

	Gpio gpio(portNumber);
	bool notReady = gpio.exportGPIO();
	gpio.setDirection("out");

	if(notReady) {
	cout << "problem with GPIO initialize" << endl;
	return 1;
	}
	if(!isStandalone && !isDaemon) {
	  printUsage();
	  return 1;
	}
	if(isDaemon) {
		cout << "daemon mode activate";
	    if(daemon(0,0)) {
	       printUsage("daemonize error");
	       gpio.unexportGPIO();
	       return 1;
          }
	}
	int stick = 1;
	char value[] = " ";
	while(!isExit) {
		value[0] = '0'+stick;
		gpio.setValue(value);
		stick=1-stick;
		cout << ".";
		cout.flush();
		sleep(3);
	}
	gpio.unexportGPIO();
	cout << "process terminated";
}




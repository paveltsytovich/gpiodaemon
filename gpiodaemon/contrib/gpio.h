#ifndef GPIO_CLASS_H
#define GPIO_CLASS_H

#include <string>
using namespace std;
class Gpio
{
 public:
   Gpio();
   Gpio(string pin);
   int exportGPIO();
   int unexportGPIO();
   int setDirection(string dir);
   int setValue(string  value);
   int getValue(string& value);
   string getPin() { return pin; }
private:
   string pin;
}; 

#endif

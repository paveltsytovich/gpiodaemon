#include <fstream>
#include <string>
#include <iostream>
#include <sstream>

#include "gpio.h"

using namespace std;

/**
 Default constructor

Use pin number 17
*/
Gpio::Gpio() : pin("17")
{
}
/**

Construct with custom pin

*/
Gpio::Gpio(string pin): pin(pin)
{
}
/**
Export gpio pin

*/
int Gpio::exportGPIO()
{
 string export_str = "/sys/class/gpio/export";
 ofstream exportgpio(export_str.c_str());
 if(exportgpio < 0)
  {
    cout << "Unable to export GPIO" << endl;
    return -1;
  }
 exportgpio <<  pin;
 exportgpio.close();
 return 0;
}
/**
Unexport GPIO
*/
int Gpio::unexportGPIO()
{
 string unexport_str = "/sys/class/gpio/unexport";
 ofstream unexportgpio(unexport_str.c_str());
 if(unexportgpio < 0)
  {
   cout << "Unable to unexport GPIO" << endl;
   return -1;
  }
unexportgpio << pin;
unexportgpio.close();
return 0;
}
/**
Set direction for GPIO
*/
int Gpio::setDirection(string dir)
{
 string direction_str =  "/sys/class/gpio/gpio" + pin + "/direction";
 ofstream direction(direction_str.c_str());
 if(direction < 0)
  {
   cout << "failed set direction for GPIO " << endl;
   return -1;
  }
direction << dir;
direction.close();
return 0;
}
/**
Get value from GPIO pin
*/
int Gpio::getValue(string &value)
{
string getval_str = "/sys/class/gpio/gpio" + pin + "/value";
ifstream  getval(getval_str.c_str());
if(getval < 0)
 {
   cout << "Unable to get value from GPIO" << endl;
   return -1;
 }
getval >> value;
if(value != "0")
 value = "1";
else
 value = "0";

getval.close();
return 0;
}
/**
Set value to GPIO
*/
int Gpio::setValue(string value)
{
 string setval_str = "/sys/class/gpio/gpio" + pin + "/value";
 ofstream setval(setval_str.c_str());
 if(setval < 0)
  {
    cout << "Unable to set value to GPIO" << endl;
    return -1;
  }
 setval << value;
 setval.close();
 return 0;
}

